from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class UpdateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class DeleteForm(ModelForm):
    class Meta:
        model = TodoList
        fields = []


class CreateItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


class UpdateItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
