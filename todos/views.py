from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import CreateForm, UpdateForm, CreateItem, UpdateItem


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_list_list": todo_lists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    todo_list_item = TodoItem.objects.all()
    context = {
        "todo_list_detail": todo_list_detail,
        "todo_list_item": todo_list_item,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            todo_list_detail = form.save()
            return redirect("todo_list_detail", id=todo_list_detail.id)
    else:
        form = CreateForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = UpdateForm(request.POST, instance=todo_instance)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = UpdateForm(instance=todo_instance)

    context = {"form": form}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = CreateItem(request.POST)
        if form.is_valid():
            create_item_form = form.save(commit=False)
            create_item_form.save()
            return redirect("todo_list_detail", id=create_item_form.list.id)
    else:
        create_item_form = CreateItem()
    context = {"create_item_form": create_item_form}
    return render(request, "todos/item_create.html", context)


def update_todo_item(request, id):
    item_instance = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = UpdateItem(request.POST, instance=item_instance)
        if form.is_valid():
            item_instance = form.save()
            return redirect("todo_list_detail", id=item_instance.id)
    else:
        form = UpdateItem(instance=item_instance)
    context = {"form": form}
    return render(request, "todos/item_update.html", context)
